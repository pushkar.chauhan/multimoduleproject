package com.company.service;

import com.company.domain.AWSS3Object;
import org.springframework.web.multipart.MultipartFile;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import javax.validation.constraints.NotNull;

public interface ObjectStorageService {
    Mono<byte[]> getByteObject(@NotNull String key);

    Mono<Void> deleteObject(@NotNull String objectKey);

    Flux<AWSS3Object> getObjects();

    Mono<String> uploadFile(MultipartFile file);
}
